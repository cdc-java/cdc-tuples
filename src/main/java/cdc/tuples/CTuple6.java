package cdc.tuples;

/**
 * Tuple of 6 comparable values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 0 type.
 * @param <T1> Value 1 type.
 * @param <T2> Value 2 type.
 * @param <T3> Value 3 type.
 * @param <T4> Value 4 type.
 * @param <T5> Value 5 type.
 */
public class CTuple6<T0 extends Comparable<? super T0>,
                     T1 extends Comparable<? super T1>,
                     T2 extends Comparable<? super T2>,
                     T3 extends Comparable<? super T3>,
                     T4 extends Comparable<? super T4>,
                     T5 extends Comparable<? super T5>>
        extends Tuple6<T0, T1, T2, T3, T4, T5>
        implements CTuple<CTuple6<T0, T1, T2, T3, T4, T5>> {
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public CTuple6(T0 value0,
                   T1 value1,
                   T2 value2,
                   T3 value3,
                   T4 value4,
                   T5 value5) {
        super(value0, value1, value2, value3, value4, value5);
    }

    public static <T0 extends Comparable<? super T0>,
                   T1 extends Comparable<? super T1>,
                   T2 extends Comparable<? super T2>,
                   T3 extends Comparable<? super T3>,
                   T4 extends Comparable<? super T4>,
                   T5 extends Comparable<? super T5>>
            CTuple6<T0, T1, T2, T3, T4, T5> of(T0 value0,
                                               T1 value1,
                                               T2 value2,
                                               T3 value3,
                                               T4 value4,
                                               T5 value5) {
        return new CTuple6<>(value0,
                             value1,
                             value2,
                             value3,
                             value4,
                             value5);
    }

    @Override
    public Comparable<?> value(int index) {
        return (Comparable<?>) super.value(index);
    }

    @Override
    public int compareTo(CTuple6<T0, T1, T2, T3, T4, T5> other) {
        final int cmp0 = CTuple.compare(value0, other.value0);
        if (cmp0 == 0) {
            final int cmp1 = CTuple.compare(value1, other.value1);
            if (cmp1 == 0) {
                final int cmp2 = CTuple.compare(value2, other.value2);
                if (cmp2 == 0) {
                    final int cmp3 = CTuple.compare(value3, other.value3);
                    if (cmp3 == 0) {
                        final int cmp4 = CTuple.compare(value4, other.value4);
                        if (cmp4 == 0) {
                            return CTuple.compare(value5, other.value5);
                        } else {
                            return cmp4;
                        }
                    } else {
                        return cmp3;
                    }
                } else {
                    return cmp2;
                }
            } else {
                return cmp1;
            }
        } else {
            return cmp0;
        }
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}