package cdc.tuples;

import java.util.Objects;

/**
 * Tuple of 5 values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 0 type.
 * @param <T1> Value 1 type.
 * @param <T2> Value 2 type.
 * @param <T3> Value 3 type.
 * @param <T4> Value 4 type.
 */
public class Tuple5<T0, T1, T2, T3, T4> implements Tuple {
    protected final T0 value0;
    protected final T1 value1;
    protected final T2 value2;
    protected final T3 value3;
    protected final T4 value4;

    @Deprecated(since = "2023-11-05", forRemoval = true)
    public Tuple5(T0 value0,
                  T1 value1,
                  T2 value2,
                  T3 value3,
                  T4 value4) {
        this.value0 = value0;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
    }

    public static <T0, T1, T2, T3, T4>
            Tuple5<T0, T1, T2, T3, T4> of(T0 value0,
                                          T1 value1,
                                          T2 value2,
                                          T3 value3,
                                          T4 value4) {
        return new Tuple5<>(value0,
                            value1,
                            value2,
                            value3,
                            value4);
    }

    @Override
    public final int size() {
        return 5;
    }

    @Override
    public Object value(int index) {
        switch (index) {
        case 0:
            return value0;
        case 1:
            return value1;
        case 2:
            return value2;
        case 3:
            return value3;
        case 4:
            return value4;
        default:
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * @return Value 0.
     */
    public final T0 value0() {
        return value0;
    }

    /**
     * @return Value 1.
     */
    public final T1 value1() {
        return value1;
    }

    /**
     * @return Value 2.
     */
    public final T2 value2() {
        return value2;
    }

    /**
     * @return Value 3.
     */
    public final T3 value3() {
        return value3;
    }

    /**
     * @return Value 4.
     */
    public final T4 value4() {
        return value4;
    }

    /**
     * @return Value 0.
     * @deprecated Use {@link #value0()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T0 getValue0() {
        return value0;
    }

    /**
     * @return Value 1.
     * @deprecated Use {@link #value1()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T1 getValue1() {
        return value1;
    }

    /**
     * @return Value 2.
     * @deprecated Use {@link #value2()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T2 getValue2() {
        return value2;
    }

    /**
     * @return Value 3.
     * @deprecated Use {@link #value3()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T3 getValue3() {
        return value3;
    }

    /**
     * @return Value 4.
     * @deprecated Use {@link #value4()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T4 geValue4() {
        return value4;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Tuple5<?, ?, ?, ?, ?>)) {
            return false;
        }
        final Tuple5<?, ?, ?, ?, ?> o = (Tuple5<?, ?, ?, ?, ?>) other;
        return Objects.equals(value0, o.value0)
                && Objects.equals(value1, o.value1)
                && Objects.equals(value2, o.value2)
                && Objects.equals(value3, o.value3)
                && Objects.equals(value4, o.value4);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value0,
                            value1,
                            value2,
                            value3,
                            value4);
    }

    @Override
    public String toString() {
        return "[" + Objects.toString(value0)
                + ", " + Objects.toString(value1)
                + ", " + Objects.toString(value2)
                + ", " + Objects.toString(value3)
                + ", " + Objects.toString(value4)
                + "]";
    }
}