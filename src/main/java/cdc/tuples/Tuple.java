package cdc.tuples;

/**
 * Base tuple interface.
 *
 * @author Damien Carbonne
 *
 */
public interface Tuple {
    /**
     * @return The tuple size.
     */
    public int size();

    /**
     * Returns the value at an index.
     *
     * @param index The index.
     * @return Value at {@code index}.
     * @throws IndexOutOfBoundsException When {@code index} is not in {@code [0, size()[} range.
     */
    public Object value(int index);

    /**
     * Returns the value at an index.
     *
     * @param index The index.
     * @return Value at {@code index}.
     * @throws IndexOutOfBoundsException When {@code index} is not in {@code [0, size()[} range.
     * @deprecated Use {@link #value(int)}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public default Object getValue(int index) {
        return value(index);
    }
}