package cdc.tuples;

import java.util.Objects;

/**
 * Tuple of 3 values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 1 type.
 * @param <T1> Value 2 type.
 * @param <T2> Value 3 type.
 */
public class Tuple3<T0, T1, T2> implements Tuple {
    protected final T0 value0;
    protected final T1 value1;
    protected final T2 value2;

    @Deprecated(since = "2023-11-05", forRemoval = true)
    public Tuple3(T0 value1,
                  T1 value2,
                  T2 value3) {
        this.value0 = value1;
        this.value1 = value2;
        this.value2 = value3;
    }

    public static <T0, T1, T2>
            Tuple3<T0, T1, T2> of(T0 value0,
                                  T1 value1,
                                  T2 value2) {
        return new Tuple3<>(value0,
                            value1,
                            value2);
    }

    @Override
    public final int size() {
        return 3;
    }

    @Override
    public Object value(int index) {
        switch (index) {
        case 0:
            return value0;
        case 1:
            return value1;
        case 2:
            return value2;
        default:
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * @return Value 0.
     */
    public final T0 value0() {
        return value0;
    }

    /**
     * @return Value 1.
     */
    public final T1 value1() {
        return value1;
    }

    /**
     * @return Value 2.
     */
    public final T2 value2() {
        return value2;
    }

    /**
     * @return Value 0.
     * @deprecated Use {@link #value0()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T0 getValue0() {
        return value0;
    }

    /**
     * @return Value 1.
     * @deprecated Use {@link #value1()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T1 getValue1() {
        return value1;
    }

    /**
     * @return Value 2.
     * @deprecated Use {@link #value2()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T2 getValue2() {
        return value2;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Tuple3<?, ?, ?>)) {
            return false;
        }
        final Tuple3<?, ?, ?> o = (Tuple3<?, ?, ?>) other;
        return Objects.equals(value0, o.value0)
                && Objects.equals(value1, o.value1)
                && Objects.equals(value2, o.value2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value0,
                            value1,
                            value2);
    }

    @Override
    public String toString() {
        return "[" + Objects.toString(value0)
                + ", " + Objects.toString(value1)
                + ", " + Objects.toString(value2)
                + "]";
    }
}