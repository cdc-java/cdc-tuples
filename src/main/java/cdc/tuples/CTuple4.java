package cdc.tuples;

/**
 * Tuple of 4 comparable values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 0 type.
 * @param <T1> Value 1 type.
 * @param <T2> Value 2 type.
 * @param <T3> Value 3 type.
 */
public class CTuple4<T0 extends Comparable<? super T0>,
                     T1 extends Comparable<? super T1>,
                     T2 extends Comparable<? super T2>,
                     T3 extends Comparable<? super T3>>
        extends Tuple4<T0, T1, T2, T3>
        implements CTuple<CTuple4<T0, T1, T2, T3>> {
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public CTuple4(T0 value0,
                   T1 value1,
                   T2 value2,
                   T3 value4) {
        super(value0, value1, value2, value4);
    }

    public static <T0 extends Comparable<? super T0>,
                   T1 extends Comparable<? super T1>,
                   T2 extends Comparable<? super T2>,
                   T3 extends Comparable<? super T3>>
            CTuple4<T0, T1, T2, T3> of(T0 value0,
                                       T1 value1,
                                       T2 value2,
                                       T3 value3) {
        return new CTuple4<>(value0,
                             value1,
                             value2,
                             value3);
    }

    @Override
    public Comparable<?> value(int index) {
        return (Comparable<?>) super.value(index);
    }

    @Override
    public int compareTo(CTuple4<T0, T1, T2, T3> other) {
        final int cmp0 = CTuple.compare(value0, other.value0);
        if (cmp0 == 0) {
            final int cmp1 = CTuple.compare(value1, other.value1);
            if (cmp1 == 0) {
                final int cmp2 = CTuple.compare(value2, other.value2);
                if (cmp2 == 0) {
                    return CTuple.compare(value3, other.value3);
                } else {
                    return cmp2;
                }
            } else {
                return cmp1;
            }
        } else {
            return cmp0;
        }
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}