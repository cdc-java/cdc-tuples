package cdc.tuples;

/**
 * Tuple of 3 comparable values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 0 type.
 * @param <T1> Value 1 type.
 * @param <T2> Value 2 type.
 */
public class CTuple3<T0 extends Comparable<? super T0>,
                     T1 extends Comparable<? super T1>,
                     T2 extends Comparable<? super T2>>
        extends Tuple3<T0, T1, T2> implements CTuple<CTuple3<T0, T1, T2>> {
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public CTuple3(T0 value0,
                   T1 value1,
                   T2 value2) {
        super(value0, value1, value2);
    }

    public static <T0 extends Comparable<? super T0>,
                   T1 extends Comparable<? super T1>,
                   T2 extends Comparable<? super T2>>
            CTuple3<T0, T1, T2> of(T0 value0,
                                   T1 value1,
                                   T2 value2) {
        return new CTuple3<>(value0,
                             value1,
                             value2);
    }

    @Override
    public Comparable<?> value(int index) {
        return (Comparable<?>) super.value(index);
    }

    @Override
    public int compareTo(CTuple3<T0, T1, T2> other) {
        final int cmp0 = CTuple.compare(value0, other.value0);
        if (cmp0 == 0) {
            final int cmp1 = CTuple.compare(value1, other.value1);
            if (cmp1 == 0) {
                return CTuple.compare(value2, other.value2);
            } else {
                return cmp1;
            }
        } else {
            return cmp0;
        }
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}