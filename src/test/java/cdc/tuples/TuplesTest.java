package cdc.tuples;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Comparator;

import org.junit.jupiter.api.Test;

class TuplesTest {
    @Test
    void testTupleNComparator() {
        final Comparator<String> c = (s1,
                                      s2) -> (Integer.parseInt(s1) - Integer.parseInt(s2));
        final Comparator<TupleN<String>> comparator = TupleN.comparator(c);

        final TupleN<String> t1 = TupleN.of("1");
        final TupleN<String> t2 = TupleN.of("2");
        final TupleN<String> t10 = TupleN.of("10");
        final TupleN<String> t100 = TupleN.of("100");

        assertSame(0, comparator.compare(t1, t1));
        assertTrue(comparator.compare(t1, t2) < 0);
        assertTrue(comparator.compare(t1, t10) < 0);
        assertTrue(comparator.compare(t1, t100) < 0);
        assertTrue(comparator.compare(t2, t1) > 0);
        assertTrue(comparator.compare(t2, t10) < 0);
        assertTrue(comparator.compare(t2, t100) < 0);
    }

    @Test
    void testCTupleN() {
        final CTupleN<String> v0 = CTupleN.of();
        final CTupleN<String> v1 = CTupleN.of("Hello");
        final CTupleN<String> v2 = CTupleN.of("Hello", "World");
        assertEquals(v0, v0);
        assertNotEquals(v0, null);
        assertNotEquals(v1, v2);
        assertSame(0, v0.compareTo(v0));
        assertTrue(v0.compareTo(v1) < 0);
        assertTrue(v0.compareTo(v2) < 0);

        assertTrue(v1.compareTo(v0) > 0);
        assertSame(0, v1.compareTo(v1));
        assertTrue(v1.compareTo(v2) < 0);

        assertTrue(v2.compareTo(v0) > 0);
        assertTrue(v2.compareTo(v1) > 0);
        assertSame(0, v2.compareTo(v2));

        assertThrows(IndexOutOfBoundsException.class, () -> {
            v0.value(-1);
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            v0.value(1);
        });
    }

    @Test
    void testCTuple2() {
        final CTuple2<String, Integer> v0 = CTuple2.of("Hello", 10);
        final CTuple2<String, Integer> v1 = CTuple2.of("Hello", 10);
        final CTuple2<String, Integer> v2 = CTuple2.of("Hello World", 10);
        assertEquals(v0, v0);
        assertNotEquals(v0, null);
        assertEquals(v0, v1);
        assertNotEquals(v0, v2);
        assertEquals("Hello", v0.value0());
        assertEquals(10, v0.value1());
        assertEquals(v0.value0(), v0.value(0));
        assertEquals(v0.value1(), v0.value(1));
        assertEquals(0, v0.compareTo(v1));
        assertTrue(v0.compareTo(v2) < 0);
        assertTrue(v2.compareTo(v0) > 0);
        assertSame(2, v0.size());
        assertThrows(IndexOutOfBoundsException.class, () -> {
            v0.value(-1);
        });
    }

    @Test
    void testCTuple3() {
        final CTuple3<String, Integer, Double> v0 = CTuple3.of("Hello", 10, 0.0);
        final CTuple3<String, Integer, Double> v1 = CTuple3.of("Hello", 10, 0.0);
        final CTuple3<String, Integer, Double> v2 = CTuple3.of("Hello World", 10, 0.0);
        assertEquals(v0, v0);
        assertNotEquals(v0, null);
        assertEquals(v0, v1);
        assertNotEquals(v0, v2);
        assertEquals("Hello", v0.value0());
        assertEquals(10, v0.value1());
        assertEquals(0.0, v0.value2());
        assertEquals(v0.value0(), v0.value(0));
        assertEquals(v0.value1(), v0.value(1));
        assertEquals(v0.value2(), v0.value(2));
        assertEquals(0, v0.compareTo(v1));
        assertTrue(v0.compareTo(v2) < 0);
        assertSame(3, v0.size());
        assertThrows(IndexOutOfBoundsException.class, () -> {
            v0.value(-1);
        });
    }

    @Test
    void testCTuple4() {
        final CTuple4<String, Integer, Double, Boolean> v0 = CTuple4.of("Hello", 10, 0.0, true);
        final CTuple4<String, Integer, Double, Boolean> v1 = CTuple4.of("Hello", 10, 0.0, true);
        final CTuple4<String, Integer, Double, Boolean> v2 = CTuple4.of("Hello World", 10, 0.0, false);
        assertEquals(v0, v0);
        assertNotEquals(v0, null);
        assertEquals(v0, v1);
        assertNotEquals(v0, v2);
        assertEquals("Hello", v0.value0());
        assertEquals(10, v0.value1());
        assertEquals(0.0, v0.value2());
        assertEquals(true, v0.value3());
        assertEquals(v0.value0(), v0.value(0));
        assertEquals(v0.value1(), v0.value(1));
        assertEquals(v0.value2(), v0.value(2));
        assertEquals(v0.value3(), v0.value(3));
        assertSame(4, v0.size());
        assertThrows(IndexOutOfBoundsException.class, () -> {
            v0.value(-1);
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            v0.value(5);
        });
    }

    @Test
    void testCTuple5() {
        final CTuple5<String, Integer, Double, Boolean, Character> v0 = CTuple5.of("Hello", 10, 0.0, true, 'a');
        final CTuple5<String, Integer, Double, Boolean, Character> v1 = CTuple5.of("Hello", 10, 0.0, true, 'a');
        final CTuple5<String, Integer, Double, Boolean, Character> v2 = CTuple5.of("Hello World", 10, 0.0, false, 'b');
        assertEquals(v0, v0);
        assertNotEquals(v0, null);
        assertEquals(v0, v1);
        assertNotEquals(v0, v2);
        assertEquals("Hello", v0.value0());
        assertEquals(10, v0.value1());
        assertEquals(0.0, v0.value2());
        assertEquals(true, v0.value3());
        assertEquals('a', v0.value4());
        assertEquals(v0.value0(), v0.value(0));
        assertEquals(v0.value1(), v0.value(1));
        assertEquals(v0.value2(), v0.value(2));
        assertEquals(v0.value3(), v0.value(3));
        assertEquals(v0.value4(), v0.value(4));
        assertSame(5, v0.size());
        assertThrows(IndexOutOfBoundsException.class, () -> {
            v0.value(-1);
        });
    }
}