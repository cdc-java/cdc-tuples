package cdc.tuples;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.junit.jupiter.api.Test;

class TupleNTest {
    private static TupleN<Integer> t(Integer... values) {
        return TupleN.of(values);
    }

    private static TupleN<Character> t(String s) {
        final List<Character> chars = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            chars.add(s.charAt(i));
        }
        return TupleN.of(chars.toArray(new Character[chars.size()]));
    }

    private static int sign(int i) {
        if (i < 0) {
            return -1;
        } else if (i > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @Test
    void testComparator0() {
        final Comparator<TupleN<Character>> comparator = TupleN.comparator(Character::compareTo);
        final String[] strings = { "", "a", "b", "aa", "ab", "ac", "ba", "bb", "bc", "ca", "cb", "cc", "abc", "abcd" };
        for (final String s1 : strings) {
            final TupleN<Character> t1 = t(s1);
            for (final String s2 : strings) {
                final TupleN<Character> t2 = t(s2);
                final int cmpt = comparator.compare(t1, t2);
                final int cmps = s1.compareTo(s2);
                assertEquals(sign(cmps), sign(cmpt));
            }
        }
    }

    @Test
    void testComparator1() {
        final Comparator<TupleN<Integer>> comparator = TupleN.comparator(Integer::compareTo);
        assertEquals(0, comparator.compare(t(), t()));
        assertEquals(0, comparator.compare(t(1), t(1)));
        assertEquals(0, comparator.compare(t(1, 1), t(1, 1)));

        assertTrue(comparator.compare(t(1), t(2)) < 0);
        assertTrue(comparator.compare(t(2), t(1)) > 0);

        assertTrue(comparator.compare(t(1), t()) > 0);
        assertTrue(comparator.compare(t(), t(1)) < 0);
        assertTrue(comparator.compare(t(1, 1), t()) > 0);
        assertTrue(comparator.compare(t(), t(1, 1)) < 0);

        assertTrue(comparator.compare(t(1, 2), t(1)) > 0);
        assertTrue(comparator.compare(t(1, 0), t(1)) > 0);
        assertTrue(comparator.compare(t(1, 2), t(2)) < 0);
        assertTrue(comparator.compare(t(1, 0), t(2)) < 0);

        assertTrue(comparator.compare(t(1), t(1, 2)) < 0);
        assertTrue(comparator.compare(t(1), t(1, 0)) < 0);
        assertTrue(comparator.compare(t(2), t(1, 2)) > 0);
        assertTrue(comparator.compare(t(2), t(1, 0)) > 0);

        assertTrue(comparator.compare(t(1, 1), t(1, 2)) < 0);
    }
}