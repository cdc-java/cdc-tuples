package cdc.tuples;

/**
 * Tuple of 9 comparable values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 0 type.
 * @param <T1> Value 1 type.
 * @param <T2> Value 2 type.
 * @param <T3> Value 3 type.
 * @param <T4> Value 4 type.
 * @param <T5> Value 5 type.
 * @param <T6> Value 6 type.
 * @param <T7> Value 7 type.
 * @param <T8> Value 8 type.
 */
public class CTuple9<T0 extends Comparable<? super T0>,
                     T1 extends Comparable<? super T1>,
                     T2 extends Comparable<? super T2>,
                     T3 extends Comparable<? super T3>,
                     T4 extends Comparable<? super T4>,
                     T5 extends Comparable<? super T5>,
                     T6 extends Comparable<? super T6>,
                     T7 extends Comparable<? super T7>,
                     T8 extends Comparable<? super T8>>
        extends Tuple9<T0, T1, T2, T3, T4, T5, T6, T7, T8>
        implements CTuple<CTuple9<T0, T1, T2, T3, T4, T5, T6, T7, T8>> {
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public CTuple9(T0 value0,
                   T1 value1,
                   T2 value2,
                   T3 value3,
                   T4 value4,
                   T5 value5,
                   T6 value6,
                   T7 value7,
                   T8 value8) {
        super(value0, value1, value2, value3, value4, value5, value6, value7, value8);
    }

    public static <T0 extends Comparable<? super T0>,
                   T1 extends Comparable<? super T1>,
                   T2 extends Comparable<? super T2>,
                   T3 extends Comparable<? super T3>,
                   T4 extends Comparable<? super T4>,
                   T5 extends Comparable<? super T5>,
                   T6 extends Comparable<? super T6>,
                   T7 extends Comparable<? super T7>,
                   T8 extends Comparable<? super T8>>
            CTuple9<T0, T1, T2, T3, T4, T5, T6, T7, T8> of(T0 value0,
                                                           T1 value1,
                                                           T2 value2,
                                                           T3 value3,
                                                           T4 value4,
                                                           T5 value5,
                                                           T6 value6,
                                                           T7 value7,
                                                           T8 value8) {
        return new CTuple9<>(value0,
                             value1,
                             value2,
                             value3,
                             value4,
                             value5,
                             value6,
                             value7,
                             value8);
    }

    @Override
    public Comparable<?> value(int index) {
        return (Comparable<?>) super.value(index);
    }

    @Override
    public int compareTo(CTuple9<T0, T1, T2, T3, T4, T5, T6, T7, T8> other) {
        final int cmp0 = CTuple.compare(value0, other.value0);
        if (cmp0 == 0) {
            final int cmp1 = CTuple.compare(value1, other.value1);
            if (cmp1 == 0) {
                final int cmp2 = CTuple.compare(value2, other.value2);
                if (cmp2 == 0) {
                    final int cmp3 = CTuple.compare(value3, other.value3);
                    if (cmp3 == 0) {
                        final int cmp4 = CTuple.compare(value4, other.value4);
                        if (cmp4 == 0) {
                            final int cmp5 = CTuple.compare(value5, other.value5);
                            if (cmp5 == 0) {
                                final int cmp6 = CTuple.compare(value6, other.value6);
                                if (cmp6 == 0) {
                                    final int cmp7 = CTuple.compare(value7, other.value7);
                                    if (cmp7 == 0) {
                                        return CTuple.compare(value8, other.value8);
                                    } else {
                                        return cmp7;
                                    }
                                } else {
                                    return cmp6;
                                }
                            } else {
                                return cmp5;
                            }
                        } else {
                            return cmp4;
                        }
                    } else {
                        return cmp3;
                    }
                } else {
                    return cmp2;
                }
            } else {
                return cmp1;
            }
        } else {
            return cmp0;
        }
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}