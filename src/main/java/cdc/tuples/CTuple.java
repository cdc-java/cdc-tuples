package cdc.tuples;

/**
 * Base interface of comparable tuples.
 *
 * @author Damien Carbonne
 *
 * @param <C> The tuple type.
 */
public interface CTuple<C extends CTuple<C>> extends Tuple, Comparable<C> {
    @Override
    public Comparable<?> value(int index);

    @Override
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public default Comparable<?> getValue(int index) {
        return value(index);
    }

    public static <T extends Comparable<? super T>> int compare(T l,
                                                                T r) {
        if (l == null) {
            if (r == null) {
                return 0;
            } else {
                return -1;
            }
        } else {
            if (r == null) {
                return 1;
            } else {
                return l.compareTo(r);
            }
        }
    }
}