package cdc.tuples;

/**
 * Tuple of 2 comparable values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 0 type.
 * @param <T1> Value 1 type.
 */
public class CTuple2<T0 extends Comparable<? super T0>,
                     T1 extends Comparable<? super T1>>
        extends Tuple2<T0, T1>
        implements CTuple<CTuple2<T0, T1>> {
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public CTuple2(T0 value0,
                   T1 value1) {
        super(value0, value1);
    }

    public static <T0 extends Comparable<? super T0>,
                   T1 extends Comparable<? super T1>>
            CTuple2<T0, T1> of(T0 value0,
                               T1 value1) {
        return new CTuple2<>(value0,
                             value1);
    }

    @Override
    public Comparable<?> value(int index) {
        return (Comparable<?>) super.value(index);
    }

    @Override
    public int compareTo(CTuple2<T0, T1> other) {
        final int cmp0 = CTuple.compare(value0, other.value0);
        if (cmp0 == 0) {
            return CTuple.compare(value1, other.value1);
        } else {
            return cmp0;
        }
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}