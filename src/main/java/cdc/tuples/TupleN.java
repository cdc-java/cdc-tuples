package cdc.tuples;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Tuple of N values, all with the same type.
 *
 * @author Damien Carbonne
 *
 * @param <T> Value type.
 */
public class TupleN<T> implements Tuple {
    private final T[] values;

    @Deprecated(since = "2023-11-05", forRemoval = true)
    @SafeVarargs
    public TupleN(T... values) {
        this.values = values.clone();
    }

    @SuppressWarnings("unchecked")
    public static <T> TupleN<T> of(T... values) {
        return new TupleN<>(values);
    }

    @Override
    public int size() {
        return values.length;
    }

    @Override
    public T value(int index) {
        if (index >= 0 && index < size()) {
            return values[index];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    @Deprecated
    public T getValue(int index) {
        return value(index);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof TupleN<?>)) {
            return false;
        }
        final TupleN<?> o = (TupleN<?>) other;
        return Arrays.equals(values, o.values);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(values);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        builder.append('[');
        for (int index = 0; index < values.length; index++) {
            if (first) {
                first = false;
            } else {
                builder.append(", ");
            }
            builder.append(values[index]);
        }
        builder.append(']');
        return builder.toString();
    }

    /**
     * Creates a lexicographic {@link Comparator} of {@code TupleN} based on a value comparator.
     *
     * @param <T> The value type.
     * @param comparator The value comparator.
     * @return A {@code TupleN<T>} Comparator based on {@code comparator}.
     */
    public static <T> Comparator<TupleN<T>> comparator(Comparator<? super T> comparator) {
        return (t1,
                t2) -> {
            final int min = Math.min(t1.size(), t2.size());
            for (int index = 0; index < min; index++) {
                final int cmp = comparator.compare(t1.value(index), t2.value(index));
                if (cmp != 0) {
                    return cmp;
                }
            }
            return t1.size() - t2.size();
        };
    }
}