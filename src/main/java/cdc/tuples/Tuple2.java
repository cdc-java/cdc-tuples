package cdc.tuples;

import java.util.Objects;

/**
 * Tuple of 2 values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 1 type.
 * @param <T1> Value 2 type.
 */
public class Tuple2<T0, T1> implements Tuple {
    protected final T0 value0;
    protected final T1 value1;

    @Deprecated(since = "2023-11-05", forRemoval = true)
    public Tuple2(T0 value0,
                  T1 value1) {
        this.value0 = value0;
        this.value1 = value1;
    }

    public static <T0, T1>
            Tuple2<T0, T1> of(T0 value0,
                              T1 value1) {
        return new Tuple2<>(value0,
                            value1);
    }

    @Override
    public final int size() {
        return 2;
    }

    @Override
    public Object value(int index) {
        switch (index) {
        case 0:
            return value0;
        case 1:
            return value1;
        default:
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * @return Value 0.
     */
    public final T0 value0() {
        return value0;
    }

    /**
     * @return Value 1.
     */
    public final T1 value1() {
        return value1;
    }

    /**
     * @return Value 0.
     * @deprecated Use {@link #value0()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T0 getValue0() {
        return value0;
    }

    /**
     * @return Value 1.
     * @deprecated Use {@link #value1()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T1 getValue1() {
        return value1;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Tuple2<?, ?>)) {
            return false;
        }
        final Tuple2<?, ?> o = (Tuple2<?, ?>) other;
        return Objects.equals(value0, o.value0)
                && Objects.equals(value1, o.value1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value0,
                            value1);
    }

    @Override
    public String toString() {
        return "[" + Objects.toString(value0) + ", "
                + Objects.toString(value1) + "]";
    }
}