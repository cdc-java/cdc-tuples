package cdc.tuples;

/**
 * Tuple of N comparable values, all with the same type.
 *
 * @author Damien Carbonne
 *
 * @param <T> Value type.
 */
public class CTupleN<T extends Comparable<? super T>> extends TupleN<T> implements CTuple<CTupleN<T>> {
    @Deprecated(since = "2023-11-05", forRemoval = true)
    @SafeVarargs
    public CTupleN(T... values) {
        super(values);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Comparable<? super T>>
            CTupleN<T> of(T... values) {
        return new CTupleN<>(values);
    }

    @Override
    public int compareTo(CTupleN<T> o) {
        final int min = Math.min(size(), o.size());
        for (int index = 0; index < min; index++) {
            final int cmp = value(index).compareTo(o.value(index));
            if (cmp != 0) {
                return cmp;
            }
        }
        return size() - o.size();
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}