package cdc.tuples;

/**
 * Tuple of 7 comparable values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 0 type.
 * @param <T1> Value 1 type.
 * @param <T2> Value 2 type.
 * @param <T3> Value 3 type.
 * @param <T4> Value 4 type.
 * @param <T5> Value 5 type.
 * @param <T6> Value 6 type.
 */
public class CTuple7<T0 extends Comparable<? super T0>,
                     T1 extends Comparable<? super T1>,
                     T2 extends Comparable<? super T2>,
                     T3 extends Comparable<? super T3>,
                     T4 extends Comparable<? super T4>,
                     T5 extends Comparable<? super T5>,
                     T6 extends Comparable<? super T6>>
        extends Tuple7<T0, T1, T2, T3, T4, T5, T6>
        implements CTuple<CTuple7<T0, T1, T2, T3, T4, T5, T6>> {
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public CTuple7(T0 value0,
                   T1 value1,
                   T2 value2,
                   T3 value3,
                   T4 value4,
                   T5 value5,
                   T6 value6) {
        super(value0, value1, value2, value3, value4, value5, value6);
    }

    public static <T0 extends Comparable<? super T0>,
                   T1 extends Comparable<? super T1>,
                   T2 extends Comparable<? super T2>,
                   T3 extends Comparable<? super T3>,
                   T4 extends Comparable<? super T4>,
                   T5 extends Comparable<? super T5>,
                   T6 extends Comparable<? super T6>>
            CTuple7<T0, T1, T2, T3, T4, T5, T6> of(T0 value0,
                                                   T1 value1,
                                                   T2 value2,
                                                   T3 value3,
                                                   T4 value4,
                                                   T5 value5,
                                                   T6 value6) {
        return new CTuple7<>(value0,
                             value1,
                             value2,
                             value3,
                             value4,
                             value5,
                             value6);
    }

    @Override
    public Comparable<?> value(int index) {
        return (Comparable<?>) super.value(index);
    }

    @Override
    public int compareTo(CTuple7<T0, T1, T2, T3, T4, T5, T6> other) {
        final int cmp0 = CTuple.compare(value0, other.value0);
        if (cmp0 == 0) {
            final int cmp1 = CTuple.compare(value1, other.value1);
            if (cmp1 == 0) {
                final int cmp2 = CTuple.compare(value2, other.value2);
                if (cmp2 == 0) {
                    final int cmp3 = CTuple.compare(value3, other.value3);
                    if (cmp3 == 0) {
                        final int cmp4 = CTuple.compare(value4, other.value4);
                        if (cmp4 == 0) {
                            final int cmp5 = CTuple.compare(value5, other.value5);
                            if (cmp5 == 0) {
                                return CTuple.compare(value6, other.value6);
                            } else {
                                return cmp5;
                            }
                        } else {
                            return cmp4;
                        }
                    } else {
                        return cmp3;
                    }
                } else {
                    return cmp2;
                }
            } else {
                return cmp1;
            }
        } else {
            return cmp0;
        }
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}