# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.4.0] - 2023-11-25
### Changed
- Moved to Java 17


## [1.3.0] - 2023-11-05
### Added
- Added unit test

### Changed
- Updated dependencies:
    - org.junit-5.10.1
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom.

### Deprecated
- To prepare use of records:
    - Deprecated `getValueX()`, use `valueX()`.
    - Deprecated `getValue(int)`, use `value(int)`.
    - Deprecated public constructors `TupleX(...)`, use static methods `TupleX.of(...)`.
    - Deprecated public constructors `CTupleX(...)`, use static methods `CTupleX.of(...)`.


## [1.2.0] - 2022-06-16
### Added
- Added `of(...)` static functions to create a tuples.


## [1.1.0] - 2022-05-19
### Added
- Added `TupleN.comparator()` to create a customized comparator.  
  For Tuple1, Tuple2, ... one can create comparators one can use standard Java functions.
 
### Changed
- Updated maven plugins
- Updated dependencies.
- Upgraded to Java 11

### Fixed
- Removed reference (property) to log4j that was not used. #1


## [1.0.0] - 2021-04-10
### Added
- First release, extracted from [cdc-utils](https://gitlab.com/cdc-java/cdc-util). cdc-java/cdc-util#36

