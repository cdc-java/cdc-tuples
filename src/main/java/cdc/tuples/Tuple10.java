package cdc.tuples;

import java.util.Objects;

/**
 * Tuple of 10 values.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 0 type.
 * @param <T1> Value 1 type.
 * @param <T2> Value 2 type.
 * @param <T3> Value 3 type.
 * @param <T4> Value 4 type.
 * @param <T5> Value 5 type.
 * @param <T6> Value 6 type.
 * @param <T7> Value 7 type.
 * @param <T8> Value 8 type.
 * @param <T9> Value 9 type.
 */
public class Tuple10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> implements Tuple {
    protected final T0 value0;
    protected final T1 value1;
    protected final T2 value2;
    protected final T3 value3;
    protected final T4 value4;
    protected final T5 value5;
    protected final T6 value6;
    protected final T7 value7;
    protected final T8 value8;
    protected final T9 value9;

    @Deprecated(since = "2023-11-05", forRemoval = true)
    public Tuple10(T0 value0,
                   T1 value1,
                   T2 value2,
                   T3 value3,
                   T4 value4,
                   T5 value5,
                   T6 value6,
                   T7 value7,
                   T8 value8,
                   T9 value9) {
        this.value0 = value0;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
        this.value5 = value5;
        this.value6 = value6;
        this.value7 = value7;
        this.value8 = value8;
        this.value9 = value9;
    }

    public static <T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>
            Tuple10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> of(T0 value0,
                                                               T1 value1,
                                                               T2 value2,
                                                               T3 value3,
                                                               T4 value4,
                                                               T5 value5,
                                                               T6 value6,
                                                               T7 value7,
                                                               T8 value8,
                                                               T9 value9) {
        return new Tuple10<>(value0,
                             value1,
                             value2,
                             value3,
                             value4,
                             value5,
                             value6,
                             value7,
                             value8,
                             value9);
    }

    @Override
    public final int size() {
        return 10;
    }

    @Override
    public Object value(int index) {
        switch (index) {
        case 0:
            return value0;
        case 1:
            return value1;
        case 2:
            return value2;
        case 3:
            return value3;
        case 4:
            return value4;
        case 5:
            return value5;
        case 6:
            return value6;
        case 7:
            return value7;
        case 8:
            return value8;
        case 9:
            return value9;
        default:
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * @return Value 0.
     */
    public final T0 value0() {
        return value0;
    }

    /**
     * @return Value 1.
     */
    public final T1 value1() {
        return value1;
    }

    /**
     * @return Value 2.
     */
    public final T2 value2() {
        return value2;
    }

    /**
     * @return Value 3.
     */
    public final T3 value3() {
        return value3;
    }

    /**
     * @return Value 4.
     */
    public final T4 value4() {
        return value4;
    }

    /**
     * @return Value 5.
     */
    public final T5 value5() {
        return value5;
    }

    /**
     * @return Value 6.
     */
    public final T6 value6() {
        return value6;
    }

    /**
     * @return Value 7.
     */
    public final T7 value7() {
        return value7;
    }

    /**
     * @return Value 8.
     */
    public final T8 value8() {
        return value8;
    }

    /**
     * @return Value 9.
     */
    public final T9 value9() {
        return value9;
    }

    /**
     * @return Value 0.
     * @deprecated Use {@link #value0()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T0 getValue0() {
        return value0;
    }

    /**
     * @return Value 1.
     * @deprecated Use {@link #value1()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T1 getValue1() {
        return value1;
    }

    /**
     * @return Value 2.
     * @deprecated Use {@link #value2()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T2 getValue2() {
        return value2;
    }

    /**
     * @return Value 3.
     * @deprecated Use {@link #value3()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T3 getValue3() {
        return value3;
    }

    /**
     * @return Value 4.
     * @deprecated Use {@link #value4()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T4 geValue4() {
        return value4;
    }

    /**
     * @return Value 5.
     * @deprecated Use {@link #value5()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T5 getValue5() {
        return value5;
    }

    /**
     * @return Value 6.
     * @deprecated Use {@link #value6()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T6 getValue6() {
        return value6;
    }

    /**
     * @return Value 7.
     * @deprecated Use {@link #value7()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T7 getValue7() {
        return value7;
    }

    /**
     * @return Value 8.
     * @deprecated Use {@link #value8()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T8 getValue8() {
        return value8;
    }

    /**
     * @return Value 9.
     * @deprecated Use {@link #value9()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T9 getValue9() {
        return value9;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Tuple10<?, ?, ?, ?, ?, ?, ?, ?, ?, ?>)) {
            return false;
        }
        final Tuple10<?, ?, ?, ?, ?, ?, ?, ?, ?, ?> o = (Tuple10<?, ?, ?, ?, ?, ?, ?, ?, ?, ?>) other;
        return Objects.equals(value0, o.value0)
                && Objects.equals(value1, o.value1)
                && Objects.equals(value2, o.value2)
                && Objects.equals(value3, o.value3)
                && Objects.equals(value4, o.value4)
                && Objects.equals(value5, o.value5)
                && Objects.equals(value6, o.value6)
                && Objects.equals(value7, o.value7)
                && Objects.equals(value8, o.value8)
                && Objects.equals(value9, o.value9);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value0,
                            value1,
                            value2,
                            value3,
                            value4,
                            value5,
                            value6,
                            value7,
                            value8,
                            value9);
    }

    @Override
    public String toString() {
        return "[" + Objects.toString(value0)
                + ", " + Objects.toString(value1)
                + ", " + Objects.toString(value2)
                + ", " + Objects.toString(value3)
                + ", " + Objects.toString(value4)
                + ", " + Objects.toString(value5)
                + ", " + Objects.toString(value6)
                + ", " + Objects.toString(value7)
                + ", " + Objects.toString(value8)
                + ", " + Objects.toString(value9)
                + "]";
    }
}