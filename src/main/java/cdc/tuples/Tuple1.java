package cdc.tuples;

import java.util.Objects;

/**
 * Tuple of 1 value.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 1 type.
 */
public class Tuple1<T0> implements Tuple {
    protected final T0 value0;

    @Deprecated(since = "2023-11-05", forRemoval = true)
    public Tuple1(T0 value0) {
        this.value0 = value0;
    }

    public static <T0>
            Tuple1<T0> of(T0 value0) {
        return new Tuple1<>(value0);
    }

    @Override
    public final int size() {
        return 1;
    }

    @Override
    public Object value(int index) {
        if (index == 0) {
            return value0;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * @return Value 0.
     */
    public final T0 value0() {
        return value0;
    }

    /**
     * @return Value 0.
     * @deprecated Use {@link #value0()}
     */
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public final T0 getValue0() {
        return value0;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Tuple1<?>)) {
            return false;
        }
        final Tuple1<?> o = (Tuple1<?>) other;
        return Objects.equals(value0, o.value0);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value0);
    }

    @Override
    public String toString() {
        return "[" + Objects.toString(value0) + "]";
    }
}