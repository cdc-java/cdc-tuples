package cdc.tuples;

/**
 * Tuple of 1 comparable value.
 *
 * @author Damien Carbonne
 *
 * @param <T0> Value 0 type.
 */
public class CTuple1<T0 extends Comparable<? super T0>> extends Tuple1<T0> implements CTuple<CTuple1<T0>> {
    @Deprecated(since = "2023-11-05", forRemoval = true)
    public CTuple1(T0 value0) {
        super(value0);
    }

    public static <T0 extends Comparable<? super T0>>
            CTuple1<T0> of(T0 value0) {
        return new CTuple1<>(value0);
    }

    @Override
    public Comparable<?> value(int index) {
        return (Comparable<?>) super.value(index);
    }

    @Override
    public int compareTo(CTuple1<T0> other) {
        return CTuple.compare(value0, other.value0);
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}